## This is Frontend Project

## Domain

Use for this project - http://127.0.0.1:8000 domain

Add BACK_DOMAIN='http://127.0.0.1:8001 domain in .env file for Backend Project

## Registration Page

/register

## Login Page

/login

## Logout

/logout

## Profile Page

/profile
