<?php

namespace App\Contracts;

interface UserServiceInterface
{

    public function registerUser($request);

    public function loginUser($request);

    public function getTokenAndAccess($data, $url, $type);

}
