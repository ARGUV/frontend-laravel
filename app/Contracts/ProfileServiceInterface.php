<?php

namespace App\Contracts;

interface ProfileServiceInterface
{

    public function profileUser($data);

    public function getTokenAndAccess($data, $url);

}
