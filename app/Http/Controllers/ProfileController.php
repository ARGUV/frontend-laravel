<?php

namespace App\Http\Controllers;

use App\Contracts\ProfileServiceInterface;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class ProfileController extends Controller
{
    private $profileService;

    /**
     * ProfileController constructor.
     *
     * @param ProfileServiceInterface $profileService
     */
    public function __construct(ProfileServiceInterface $profileService)
    {
        $this->profileService = $profileService;
    }

    /**
     * Profile of User
     *
     * @param $request
     */
    public function profile(Request $request)
    {
        $user = $request->session()->get('data');
        if ($user) {
            $result = $this->profileService->profileUser($user);

            return view('Profile.profile',
                [
                    'result' => $result['data'],
                ]);
        }

        return Redirect::to("login");
    }
}
