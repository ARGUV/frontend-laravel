<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller as Controller;
use App\Contracts\UserServiceInterface;
use Illuminate\Support\Facades\Redirect;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Auth\Authenticatable;

class UserController extends Controller
{
    private $userService;

    /**
     * UserController constructor.
     *
     * @param UserServiceInterface $userService
     */
    public function __construct(UserServiceInterface $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Register View
     *
     */
    public function register()
    {
        return view('User.register',
            [
                'title' => 'Register Page',
            ]);
    }

    /**
     * Login View
     *
     */
    public function login()
    {
        return view('User.login',
            [
                'title' => 'Login Page',
            ]);
    }

    /**
     * Register Post
     *
     * @param $request
     */
    public function registerPost(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:7|confirmed',
            'password_confirmation' => 'required|same:password',
        ]);

        $result = $this->userService->registerUser($request);

        if ($result['success']) {
            $request->session()->put('data', $result['data']);
        }

        return Redirect::to("profile");
    }

    /**
     * Login Post
     *
     * @param $request
     */
    public function loginPost(Request $request)
    {
        $result = $this->userService->loginUser($request);

        if ($result['success']) {
            $request->session()->put('data', $result['data']);
        }

        return Redirect::to("profile");
    }

    /**
     * Logout
     *
     * @param $request
     */
    public function logout(Request $request)
    {
        if ($request->session()->has('data')) {
            $user = $request->session()->get('data');

            $result = $this->userService->logoutUser($user);

            if ($result['success']) {
                $request->session()->forget('data');
            }
        }
        return Redirect::to("login");
    }

}
