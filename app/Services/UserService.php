<?php

namespace App\Services;

use App\Contracts\UserServiceInterface;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;

class UserService implements UserServiceInterface
{
    private $domain;

    private $client;

    /**
     * UserService constructor.
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
        $this->domain = env("BACK_DOMAIN", 'http://127.0.0.1:8001');
    }

    /**
     * Register Service
     *
     * @param $request
     */
    public function registerUser($request)
    {
        $data = [
            'form_params' => [
                'name' => $request->name,
                'email' => $request->email,
                'password' => $request->password,
                'password_confirmation' => $request->password_confirmation,
            ]
        ];

        $url = $this->domain . '/api/register';

        return $this->getTokenAndAccess($data, $url);
    }

    /**
     * Login Service
     *
     * @param $request
     */
    public function loginUser($request)
    {
        $data = [
            'form_params' => [
                'email' => $request->email,
                'password' => $request->password,
            ]
        ];

        $url = $this->domain . '/api/login';

        return $this->getTokenAndAccess($data, $url);
    }

    /**
     * Logout Service
     *
     * @param $user
     */
    public function logoutUser($user)
    {
        $data = [
            'headers' => [
                'Authorization' => 'Bearer ' . $user['token']
            ]
        ];

        $url = $this->domain . '/api/logout';
        $type = 'GET';

        return $this->getTokenAndAccess($data, $url, $type);
    }

    /**
     * Send Request
     *
     * @param $data
     * @param $url
     * @param $type
     */
    public function getTokenAndAccess($data, $url, $type = 'POST')
    {

        $response = $this->client->request($type, $url, $data);

        return json_decode((string)$response->getBody(), true);
    }

}
