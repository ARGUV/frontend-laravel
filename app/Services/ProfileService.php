<?php

namespace App\Services;

use App\Contracts\ProfileServiceInterface;
use GuzzleHttp\Client;

class ProfileService implements ProfileServiceInterface
{
    private $domain;

    private $client;

    /**
     * UserService constructor.
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
        $this->domain = env("BACK_DOMAIN",'http://127.0.0.1:8001');
    }

    /**
     * Profile Service
     *
     * @param $user
     */
    public function profileUser($user)
    {
        $data = [
            'Authorization' => 'Bearer ' . $user['token']
        ];

        $url = $this->domain . '/api/users/' . $user['id'];

        return $this->getTokenAndAccess($data, $url);
    }

    /**
     * Send Request
     *
     * @param $data
     * @param $url
     */
    public function getTokenAndAccess($data, $url)
    {
        $response = $this->client->request('GET', $url, [
            'headers' => $data,
        ]);

        return json_decode((string)$response->getBody(), true);
    }
}
