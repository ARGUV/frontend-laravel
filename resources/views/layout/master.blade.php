<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>

<body class="page-dashboard">

<div class="row">
    <div class="container">
        @yield('content')
    </div>
</div>

</body>
</html>
