@extends('layout.master')

@section('content')

    <h1>Profile</h1>

    <table class="table">
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Address</th>
            <th>Phone Number</th>
        </tr>
        @foreach($result as $data)
            <tr>
                <td>{{ $data['name'] ?? '' }}</td>
                <td>{{ $data['email'] ?? '' }}</td>
                <td>{{ $data['address'] ?? '' }}</td>
                <td>{{ $data['phone_number'] ?? '' }}</td>
            </tr>
        @endforeach
    </table>

@endsection
